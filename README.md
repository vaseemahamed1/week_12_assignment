//Download Kalfa latest version, use below Link:
https://kafka.apache.org/downloads

store the kafka in c drive and rename the kafka folder to kafka

// Some config changes, change log folder:
Open kafka/config/server.properties
search logs and change the path to custom Path as below
log.dirs=<kafka directory>/kafka-logs
update port for linux machines as below
listeners=PLAINTEXT://:9098

Open kafka/config/zookeeper.properties
change dataDir= <kafka directory>/zookeeper.properties

You can start the Zookeeper and Kafka servers by using the below commands.
Depending on your OS and your file path, run the below commands in new CMD's
------------------
For Windows operating system
Start Zookeeper :
$ .\bin\windows\zookeeper-server-start.bat .\config\zookeeper.properties

Start Kafka server:
$ .\bin\windows\kafka-server-start.bat .\config\server.properties

---------------------
For Linux operating system:
Start Zookeeper :
$ bin\zookeeper-server-start.sh config\zookeeper.properties

Start Kafka server:
$ bin\kafka-server-start.sh config\server.properties

Start up the spring boot kafka application
Topic will be automatically created based on the project configuration

swagger can be used to do the chat operation between users and admin respectively

kafka has been downloaded and placed in assignment folder and properties files have been updated
just need to start the zookeeper and kafka server using above cmds before running the app
