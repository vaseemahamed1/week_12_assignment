package com.greatlearning.resturant;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
public class Consumer {

    @Autowired
    private MessageRepository messageRepo;

    @KafkaListener(topics = "${myapp.kafka.admin-topic}", groupId = "group_id")
    public void consumeMessageForAdmin(String message) {
        System.out.println("Message to Admin from User: " + message);
        messageRepo.addAdminMessage(message);
    }

    @KafkaListener(topics = "${myapp.kafka.user-topic}", groupId = "group_id")
    public void consumeMessageForUserFromAdmin(String message) {
        System.out.println("Message to User from Admin: " + message);
        messageRepo.addUserMessage(message);
    }

    @KafkaListener(topics = "${myapp.kafka.user-to-user-topic}", groupId = "group_id")
    public void consumeMessageFromUserToUser(String message) {
        System.out.println("Message to User from User: " + message);
        messageRepo.addUserMessageFromOtherUser(message);
    }

}
