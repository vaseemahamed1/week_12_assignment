package com.greatlearning.resturant;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class Producer {

    @Value("${myapp.kafka.admin-topic}")
    private String ADMIN_TOPIC;

    @Value("${myapp.kafka.user-topic}")
    private String USER_TOPIC;

    @Value("${myapp.kafka.user-to-user-topic}")
    private String USER_TO_USER_TOPIC;

    @Autowired
    private KafkaTemplate<String, String> kafkaTemplate;

    public void sendMessageToAdmin(String message) {
        this.kafkaTemplate.send(ADMIN_TOPIC, message);
    }

    public void sendMessageToUserFromAdmin(String message) {
        this.kafkaTemplate.send(USER_TOPIC, message);
    }

    public void sendMessageToUserFromUser(String message) {
        this.kafkaTemplate.send(USER_TO_USER_TOPIC, message);
    }

    @Bean
    public NewTopic createTopicForAdmin() {
        return new NewTopic(ADMIN_TOPIC, 1, (short) 1);
    }

    @Bean
    public NewTopic createTopicForUser() {
        return new NewTopic(USER_TOPIC, 1, (short) 1);
    }

    @Bean
    public NewTopic createTopicForUserToUser() {
        return new NewTopic(USER_TO_USER_TOPIC, 1, (short) 1);
    }
}
