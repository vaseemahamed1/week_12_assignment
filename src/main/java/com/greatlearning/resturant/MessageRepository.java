package com.greatlearning.resturant;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class MessageRepository {
    private List<String> adminMessageList = new ArrayList<>();
    private List<String> userMessageListFromAdmin = new ArrayList<>();
    private List<String> userMessageListFromOtherUser = new ArrayList<>();

    public void addAdminMessage(String message) {
        adminMessageList.add(message);
    }

    public String getAllAdminMessages() {
        return adminMessageList.toString();
    }

    public void addUserMessage(String message) {
        userMessageListFromAdmin.add(message);
    }

    public String getAllUserMessages() {
        return userMessageListFromAdmin.toString();
    }

    public void addUserMessageFromOtherUser(String message) {
        userMessageListFromOtherUser.add(message);
    }

    public String getAllUserMessagesFromOtherUser() {
        return userMessageListFromOtherUser.toString();
    }
}
